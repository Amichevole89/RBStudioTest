CREATE SCHEMA IF NOT EXISTS "public";

CREATE TABLE "public"."Campaigns" (
    "Id"                   serial  NOT NULL  ,
    "Name"                 varchar(200)  NOT NULL  ,
    "StartDate"            date  NOT NULL  ,
    "EndDate"              date  NOT NULL  ,
    CONSTRAINT "pk_Campaigns" PRIMARY KEY ( "Id" )
);

CREATE TABLE "public"."ContactTypes" (
    "Id"                   serial  NOT NULL  ,
    "Name"                 varchar(50)  NOT NULL  ,
    CONSTRAINT "pk_ContactTypes" PRIMARY KEY ( "Id" )
);

CREATE TABLE "public"."Establishments" (
    "Id"                   serial  NOT NULL  ,
    "Name"                 varchar(200)  NOT NULL  ,
    CONSTRAINT "pk_Establishments" PRIMARY KEY ( "Id" )
);

CREATE TABLE "public"."EventTypes" (
    "Id"                   serial  NOT NULL  ,
    "CampaignId"           integer  NOT NULL  ,
    "Name"                 varchar(100)  NOT NULL  ,
    "Cost"                 money  NOT NULL  ,
    "Donation"             money  NOT NULL  ,
    "MinimumDonation"      money  NOT NULL  ,
    CONSTRAINT "pk_EventTypes" PRIMARY KEY ( "Id" )
);

CREATE INDEX "Idx_EventTypes_CampaignId" ON "public"."EventTypes"  ( "CampaignId" );

CREATE TABLE "public"."Locations" (
    "Id"                   serial  NOT NULL  ,
    "EstablishmentId"      integer  NOT NULL  ,
    "Name"                 varchar(100)  NOT NULL  ,
    "Address"              varchar(500)  NOT NULL  ,
    "City"                 varchar(30) DEFAULT 'Houston' NOT NULL  ,
    "Address_state"        char(2) DEFAULT 'TX'::bpchar NOT NULL  ,
    "PostalCode"           varchar(10)  NOT NULL  ,
    CONSTRAINT "pk_Locations" PRIMARY KEY ( "Id" )
);

CREATE INDEX "Idx_Locations_EstablishmentId" ON "public"."Locations"  ( "EstablishmentId" );

CREATE TABLE "public"."People" (
    "Id"                   uuid NOT NULL,
    "Name"                 varchar(200),
    "Email"                varchar(254),
    CONSTRAINT "pk_People" PRIMARY KEY ( "Id" )
);

CREATE TABLE "public"."Accounts" (
    "PersonId"             uuid NOT NULL,
    "HashedPassword"       varchar(100) NOT NULL,
    "Active"               boolean DEFAULT true NOT NULL,
    "Internal"             boolean DEFAULT false NOT NULL,
    CONSTRAINT "pk_credentials" PRIMARY KEY ( "PersonId" )
);

CREATE TABLE "public"."CampaignLocations" (
    "Id"                   serial  NOT NULL  ,
    "LocationId"           integer  NOT NULL  ,
    "CampaignId"           integer  NOT NULL  ,
    "Notes"                text  NOT NULL  ,
    CONSTRAINT "pk_CampaignLocations" PRIMARY KEY ( "Id" )
);

CREATE INDEX "Idx_CampaignLocations_LocationId" ON "public"."CampaignLocations"  ( "LocationId" );

CREATE INDEX "Idx_CampaignLocations_CampaignId" ON "public"."CampaignLocations"  ( "CampaignId" );

CREATE TABLE "public"."Contacts" (
    "PersonId"             uuid NOT NULL,
    "EstablishmentId"      integer  NOT NULL  ,
    "ContactTypeId"        integer  NOT NULL  ,
    "Phone"                varchar(20)  NOT NULL  ,
    "Active"               boolean DEFAULT true NOT NULL  ,
    CONSTRAINT "pk_Contacts" PRIMARY KEY ( "PersonId" )
);

CREATE TABLE "public"."Sessions" (
    "Id"                   bigserial  NOT NULL  ,
    "AccountId"            uuid NOT NULL,
    "Jti"                  uuid NOT NULL,
    "RefreshToken"         uuid NOT NULL,
    "Active"               boolean DEFAULT false NOT NULL,
    "Created"              timestamptz DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "pk_Sessions" PRIMARY KEY ( "Id" ),
    CONSTRAINT "unq_Sessions_Jti" UNIQUE ( "Jti" )
);

CREATE INDEX "Idx_Sessions_Jti_RefreshToken" ON "public"."Sessions"  ( "Jti", "RefreshToken" );

CREATE TABLE "public"."CampaignLocationEvents" (
    "Id"                   serial  NOT NULL  ,
    "CampaignLocationId"   integer  NOT NULL  ,
    "ParticipantCount"     integer  NOT NULL  ,
    "Entered"              timestamptz    ,
    "Invoiced"             date    ,
    "Paid"                 date    ,
    "EventTypeId"          integer  NOT NULL  ,
    "Notes"                text    ,
    CONSTRAINT "pk_CampaignLocationEvents" PRIMARY KEY ( "Id" )
);


CREATE INDEX "Idx_CampaignLocationEvents_CampaignLocationId" ON "public"."CampaignLocationEvents"  ( "CampaignLocationId" );

CREATE INDEX "Idx_CampaignLocationEvents_EventTypeId" ON "public"."CampaignLocationEvents"  ( "EventTypeId" );

ALTER TABLE "public"."Accounts" ADD CONSTRAINT "fk_Accounts_People" FOREIGN KEY ( "PersonId" ) REFERENCES "public"."People"( "Id" ) ON DELETE CASCADE;

ALTER TABLE "public"."CampaignLocationEvents" ADD CONSTRAINT "fk_CampaignLocationEvents_CampaignLocations" FOREIGN KEY ( "CampaignLocationId" ) REFERENCES "public"."CampaignLocations"( "Id" );

ALTER TABLE "public"."CampaignLocationEvents" ADD CONSTRAINT "fk_CampaignLocationEvents_EventTypes" FOREIGN KEY ( "EventTypeId" ) REFERENCES "public"."EventTypes"( "Id" );

ALTER TABLE "public"."CampaignLocations" ADD CONSTRAINT "fk_CampaignLocations_Locations" FOREIGN KEY ( "LocationId" ) REFERENCES "public"."Locations"( "Id" );

ALTER TABLE "public"."CampaignLocations" ADD CONSTRAINT "fk_CampaignLocations_Campaigns" FOREIGN KEY ( "CampaignId" ) REFERENCES "public"."Campaigns"( "Id" );

ALTER TABLE "public"."Contacts" ADD CONSTRAINT "fk_Contacts_Establishments" FOREIGN KEY ( "EstablishmentId" ) REFERENCES "public"."Establishments"( "Id" );

ALTER TABLE "public"."Contacts" ADD CONSTRAINT "fk_Contacts_ContactTypes" FOREIGN KEY ( "ContactTypeId" ) REFERENCES "public"."ContactTypes"( "Id" );

ALTER TABLE "public"."Contacts" ADD CONSTRAINT "fk_Contacts_People" FOREIGN KEY ( "PersonId" ) REFERENCES "public"."People"( "Id" );

ALTER TABLE "public"."EventTypes" ADD CONSTRAINT "fk_EventTypes_Campaigns" FOREIGN KEY ( "CampaignId" ) REFERENCES "public"."Campaigns"( "Id" ) ON DELETE CASCADE;

ALTER TABLE "public"."Locations" ADD CONSTRAINT "fk_Locations_Establishments" FOREIGN KEY ( "EstablishmentId" ) REFERENCES "public"."Establishments"( "Id" ) ON DELETE CASCADE;

ALTER TABLE "public"."Sessions" ADD CONSTRAINT "fk_Sessions_Accounts" FOREIGN KEY ( "AccountId" ) REFERENCES "public"."Accounts"( "PersonId" ) ON DELETE CASCADE;

