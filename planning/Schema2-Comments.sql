COMMENT ON TABLE "public"."Campaigns" IS 'The fund-raising Campaigns.';

COMMENT ON COLUMN "public"."Campaigns"."Id" IS 'The database Identifier for the campaign.';

COMMENT ON COLUMN "public"."Campaigns"."Name" IS 'The Name of the campaign.';

COMMENT ON COLUMN "public"."Campaigns"."StartDate" IS 'The day the campaign starts on.';

COMMENT ON COLUMN "public"."Campaigns"."EndDate" IS 'The day the campaign ends.';

COMMENT ON TABLE "public"."ContactTypes" IS 'Enumerates the types of Contacts that a restaurant can have.';

COMMENT ON COLUMN "public"."ContactTypes"."Id" IS 'The database Identifier for the contact type.';

COMMENT ON COLUMN "public"."ContactTypes"."Name" IS 'The Name of the contact type.';

COMMENT ON TABLE "public"."Establishments" IS 'The places that can participate in a campaign';

COMMENT ON COLUMN "public"."Establishments"."Id" IS 'The database Identifier for an establishment.';

COMMENT ON COLUMN "public"."Establishments"."Name" IS 'The Name of the establishment.';

COMMENT ON TABLE "public"."EventTypes" IS 'The types of events that can happen during the campaign.';

COMMENT ON COLUMN "public"."EventTypes"."Id" IS 'The database Identifier for the event type.';

COMMENT ON COLUMN "public"."EventTypes"."CampaignId" IS 'The database Identifier of the campaign that the event type belongs to.';

COMMENT ON COLUMN "public"."EventTypes"."Name" IS 'The Name of the event type';

COMMENT ON COLUMN "public"."EventTypes"."Cost" IS 'The Cost of the event for a participant.';

COMMENT ON COLUMN "public"."EventTypes"."Donation" IS 'The value donated for each participant at the event.';

COMMENT ON COLUMN "public"."EventTypes"."MinimumDonation" IS 'The minimum Donation that must be given for the event irrespective of the number of participants.';

COMMENT ON TABLE "public"."Locations" IS 'A physical location associated with an establishment.';

COMMENT ON COLUMN "public"."Locations"."Id" IS 'The database Identifier for the location.';

COMMENT ON COLUMN "public"."Locations"."EstablishmentId" IS 'The database Identifier for the establishment that owns this location.';

COMMENT ON COLUMN "public"."Locations"."Name" IS 'The Name of the location';

COMMENT ON COLUMN "public"."Locations"."Address" IS 'The street Address of the physical location.';

COMMENT ON COLUMN "public"."Locations"."City" IS 'The City that the location is in.';

COMMENT ON COLUMN "public"."Locations"."Address_state" IS 'The state that the location exists in.';

COMMENT ON COLUMN "public"."Locations"."PostalCode" IS 'The ZIP.';

COMMENT ON TABLE "public"."People" IS 'The People that use the application.';

COMMENT ON COLUMN "public"."People"."Id" IS 'The database Identifier for the person.';

COMMENT ON COLUMN "public"."People"."Name" IS 'The person''s Name.';

COMMENT ON COLUMN "public"."People"."Email" IS 'The person''s Email.';

COMMENT ON TABLE "public"."Accounts" IS 'The Accounts for administrators to access the system.';

COMMENT ON COLUMN "public"."Accounts"."PersonId" IS 'The database Identifier for the credentials.';

COMMENT ON COLUMN "public"."Accounts"."HashedPassword" IS 'The cryptographically hashed password for the account.';

COMMENT ON COLUMN "public"."Accounts"."Internal" IS 'Indicates that the account is for a person Internal to the organization.';

COMMENT ON TABLE "public"."CampaignLocations" IS 'The establishment Locations that participate in a campaign.';

COMMENT ON COLUMN "public"."CampaignLocations"."Id" IS 'The database Identifier for the location''s participation in a campaign.';

COMMENT ON COLUMN "public"."CampaignLocations"."LocationId" IS 'The database Identifier for the location participating in the campaign.';

COMMENT ON COLUMN "public"."CampaignLocations"."CampaignId" IS 'The database Identifier for the campaign that the location is participating in.';

COMMENT ON COLUMN "public"."CampaignLocations"."Notes" IS 'Notes about the location participating in the campaign.';

COMMENT ON TABLE "public"."Contacts" IS 'The Contacts for an establishment.';

COMMENT ON COLUMN "public"."Contacts"."PersonId" IS 'The database Identifier for the specific contact from the Accounts table.';

COMMENT ON COLUMN "public"."Contacts"."EstablishmentId" IS 'The database Identifier of the establishment that the contact works for.';

COMMENT ON COLUMN "public"."Contacts"."ContactTypeId" IS 'The type of contact for the establishment.';

COMMENT ON COLUMN "public"."Contacts"."Phone" IS 'The Phone number for the contact.';

COMMENT ON COLUMN "public"."Contacts"."Active" IS 'Whether or not the contact is still Active.';

COMMENT ON INDEX "public"."Idx_Sessions_Jti_RefreshToken" IS 'Index on Jti and RefreshToken for quick lookups during the refresh workflow.';

COMMENT ON CONSTRAINT "unq_Sessions_Jti" ON "public"."Sessions" IS 'Each Jti should be unique.';

COMMENT ON TABLE "public"."Sessions" IS 'The ongoing Sessions for Accounts.';

COMMENT ON COLUMN "public"."Sessions"."Id" IS 'The database Identifier for the session.';

COMMENT ON COLUMN "public"."Sessions"."AccountId" IS 'The database Identifier for the account that is in this session.';

COMMENT ON COLUMN "public"."Sessions"."Jti" IS 'The unique Identifier for the JWT associated with this session.';

COMMENT ON COLUMN "public"."Sessions"."RefreshToken" IS 'The token that can be used to refresh this specific session.';

COMMENT ON COLUMN "public"."Sessions"."Active" IS 'A flag to indicate which session is Active for an account.';

COMMENT ON COLUMN "public"."Sessions"."Created" IS 'The timestamp when this was Created';

COMMENT ON TABLE "public"."CampaignLocationEvents" IS 'The event types that a location participating in a campaign will host.';

COMMENT ON COLUMN "public"."CampaignLocationEvents"."Id" IS 'The database Identifier for the location''s willingness to host a specific event type.';

COMMENT ON COLUMN "public"."CampaignLocationEvents"."CampaignLocationId" IS 'The database Identifier of the location''s participation in a campaign.';

COMMENT ON COLUMN "public"."CampaignLocationEvents"."ParticipantCount" IS 'The number of participants that the location hosted for the specific type of event over the course of the campaign.';

COMMENT ON COLUMN "public"."CampaignLocationEvents"."Entered" IS 'The timestamp at which point the count was Entered.';

COMMENT ON COLUMN "public"."CampaignLocationEvents"."Invoiced" IS 'The date on which the invoice was sent to the location.';

COMMENT ON COLUMN "public"."CampaignLocationEvents"."Paid" IS 'The date on which the location Paid the invoice.';

COMMENT ON COLUMN "public"."CampaignLocationEvents"."EventTypeId" IS 'The specific event type that the count applies to.';

COMMENT ON COLUMN "public"."CampaignLocationEvents"."Notes" IS 'Notes about the recorded count and the location''s work with the specific event type.';
