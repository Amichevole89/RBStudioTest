CREATE SCHEMA IF NOT EXISTS "public";

CREATE TABLE "public"."Campaigns" (
    Id                   serial  NOT NULL  ,
    Name                 varchar(200)  NOT NULL  ,
    StartDate            date  NOT NULL  ,
    EndDate              date  NOT NULL  ,
    CONSTRAINT pk_Campaigns PRIMARY KEY ( Id )
);

CREATE TABLE "public"."ContactTypes" (
    Id                   serial  NOT NULL  ,
    Name                 varchar(50)  NOT NULL  ,
    CONSTRAINT pk_ContactTypes PRIMARY KEY ( Id )
);

CREATE TABLE "public"."Establishments" (
    Id                   serial  NOT NULL  ,
    Name                 varchar(200)  NOT NULL  ,
    CONSTRAINT pk_Establishments PRIMARY KEY ( Id )
);

CREATE TABLE "public"."EventTypes" (
    Id                   serial  NOT NULL  ,
    CampaignId           integer  NOT NULL  ,
    Name                 varchar(100)  NOT NULL  ,
    Cost                 money  NOT NULL  ,
    Donation             money  NOT NULL  ,
    MinimumDonation      money  NOT NULL  ,
    CONSTRAINT pk_EventTypes PRIMARY KEY ( Id )
);

CREATE INDEX Idx_EventTypes_CampaignId ON "public"."EventTypes"  ( CampaignId );

CREATE TABLE "public"."Locations" (
    Id                   serial  NOT NULL  ,
    EstablishmentId      integer  NOT NULL  ,
    Name                 varchar(100)  NOT NULL  ,
    Address              varchar(500)  NOT NULL  ,
    City                 varchar(30) DEFAULT 'Houston' NOT NULL  ,
    Address_state        char(2) DEFAULT 'TX'::bpchar NOT NULL  ,
    PostalCode           varchar(10)  NOT NULL  ,
    CONSTRAINT pk_Locations PRIMARY KEY ( Id )
);

CREATE INDEX Idx_Locations_EstablishmentId ON "public"."Locations"  ( EstablishmentId );

CREATE TABLE "public"."People" (
    Id                   uuid NOT NULL,
    Name                 varchar(200),
    Email                varchar(254),
    CONSTRAINT pk_People PRIMARY KEY (Id)
);

CREATE TABLE "public"."Accounts" (
    PersonId             uuid NOT NULL,
    HashedPassword       varchar(100) NOT NULL,
    Active               boolean DEFAULT true NOT NULL,
    Internal             boolean DEFAULT false NOT NULL,
    CONSTRAINT pk_credentials PRIMARY KEY (PersonId)
);

CREATE TABLE "public"."CampaignLocations" (
    Id                   serial  NOT NULL  ,
    LocationId           integer  NOT NULL  ,
    CampaignId           integer  NOT NULL  ,
    Notes                text  NOT NULL  ,
    CONSTRAINT pk_CampaignLocations PRIMARY KEY ( Id )
);

CREATE INDEX Idx_CampaignLocations_LocationId ON "public"."CampaignLocations"  ( LocationId );

CREATE INDEX Idx_CampaignLocations_CampaignId ON "public"."CampaignLocations"  ( CampaignId );

CREATE TABLE "public"."Contacts" (
    PersonId             uuid NOT NULL,
    EstablishmentId      integer  NOT NULL  ,
    ContactTypeId        integer  NOT NULL  ,
    Phone                varchar(20)  NOT NULL  ,
    Active               boolean DEFAULT true NOT NULL  ,
    CONSTRAINT pk_Contacts PRIMARY KEY ( PersonId )
);

CREATE TABLE "public"."Sessions" (
    Id                   bigserial  NOT NULL  ,
    AccountId            uuid NOT NULL,
    Jti                  uuid NOT NULL,
    RefreshToken         uuid NOT NULL,
    Active               boolean DEFAULT false NOT NULL,
    Created              timestamptz DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT pk_Sessions PRIMARY KEY ( Id ),
    CONSTRAINT unq_Sessions_Jti UNIQUE ( Jti )
);

CREATE INDEX Idx_Sessions_Jti_RefreshToken ON "public"."Sessions"  ( Jti, RefreshToken );

CREATE TABLE "public"."CampaignLocationEvents" (
    Id                   serial  NOT NULL  ,
    CampaignLocationId   integer  NOT NULL  ,
    ParticipantCount     integer  NOT NULL  ,
    Entered              timestamptz    ,
    Invoiced             date    ,
    Paid                 date    ,
    EventTypeId          integer  NOT NULL  ,
    Notes                text    ,
    CONSTRAINT pk_CampaignLocationEvents PRIMARY KEY ( Id )
);


CREATE INDEX Idx_CampaignLocationEvents_CampaignLocationId ON "public"."CampaignLocationEvents"  ( CampaignLocationId );

CREATE INDEX Idx_CampaignLocationEvents_EventTypeId ON "public"."CampaignLocationEvents"  ( EventTypeId );

ALTER TABLE "public"."Accounts" ADD CONSTRAINT fk_Accounts_People FOREIGN KEY ( PersonId ) REFERENCES "public"."People"( Id ) ON DELETE CASCADE;

ALTER TABLE "public"."CampaignLocationEvents" ADD CONSTRAINT fk_CampaignLocationEvents_CampaignLocations FOREIGN KEY ( CampaignLocationId ) REFERENCES "public"."CampaignLocations"( Id );

ALTER TABLE "public"."CampaignLocationEvents" ADD CONSTRAINT fk_CampaignLocationEvents_EventTypes FOREIGN KEY ( EventTypeId ) REFERENCES "public"."EventTypes"( Id );

ALTER TABLE "public"."CampaignLocations" ADD CONSTRAINT fk_CampaignLocations_Locations FOREIGN KEY ( LocationId ) REFERENCES "public"."Locations"( Id );

ALTER TABLE "public"."CampaignLocations" ADD CONSTRAINT fk_CampaignLocations_Campaigns FOREIGN KEY ( CampaignId ) REFERENCES "public"."Campaigns"( Id );

ALTER TABLE "public"."Contacts" ADD CONSTRAINT fk_Contacts_Establishments FOREIGN KEY ( EstablishmentId ) REFERENCES "public"."Establishments"( Id );

ALTER TABLE "public"."Contacts" ADD CONSTRAINT fk_Contacts_ContactTypes FOREIGN KEY ( ContactTypeId ) REFERENCES "public"."ContactTypes"( Id );

ALTER TABLE "public"."Contacts" ADD CONSTRAINT fk_Contacts_People FOREIGN KEY ( PersonId ) REFERENCES "public"."People"( Id );

ALTER TABLE "public"."EventTypes" ADD CONSTRAINT fk_EventTypes_Campaigns FOREIGN KEY ( CampaignId ) REFERENCES "public"."Campaigns"( Id ) ON DELETE CASCADE;

ALTER TABLE "public"."Locations" ADD CONSTRAINT fk_Locations_Establishments FOREIGN KEY ( EstablishmentId ) REFERENCES "public"."Establishments"( Id ) ON DELETE CASCADE;

ALTER TABLE "public"."Sessions" ADD CONSTRAINT fk_Sessions_Accounts FOREIGN KEY ( AccountId ) REFERENCES "public"."Accounts"( PersonId ) ON DELETE CASCADE;

COMMENT ON TABLE "public"."Campaigns" IS 'The fund-raising Campaigns.';

COMMENT ON COLUMN "public"."Campaigns".Id IS 'The database Identifier for the campaign.';

COMMENT ON COLUMN "public"."Campaigns".Name IS 'The Name of the campaign.';

COMMENT ON COLUMN "public"."Campaigns".StartDate IS 'The day the campaign starts on.';

COMMENT ON COLUMN "public"."Campaigns".EndDate IS 'The day the campaign ends.';

COMMENT ON TABLE "public"."ContactTypes" IS 'Enumerates the types of Contacts that a restaurant can have.';

COMMENT ON COLUMN "public"."ContactTypes".Id IS 'The database Identifier for the contact type.';

COMMENT ON COLUMN "public"."ContactTypes".Name IS 'The Name of the contact type.';

COMMENT ON TABLE "public"."Establishments" IS 'The places that can participate in a campaign';

COMMENT ON COLUMN "public"."Establishments".Id IS 'The database Identifier for an establishment.';

COMMENT ON COLUMN "public"."Establishments".Name IS 'The Name of the establishment.';

COMMENT ON TABLE "public"."EventTypes" IS 'The types of events that can happen during the campaign.';

COMMENT ON COLUMN "public"."EventTypes".Id IS 'The database Identifier for the event type.';

COMMENT ON COLUMN "public"."EventTypes".CampaignId IS 'The database Identifier of the campaign that the event type belongs to.';

COMMENT ON COLUMN "public"."EventTypes".Name IS 'The Name of the event type';

COMMENT ON COLUMN "public"."EventTypes".Cost IS 'The Cost of the event for a participant.';

COMMENT ON COLUMN "public"."EventTypes".Donation IS 'The value donated for each participant at the event.';

COMMENT ON COLUMN "public"."EventTypes".MinimumDonation IS 'The minimum Donation that must be given for the event irrespective of the number of participants.';

COMMENT ON TABLE "public"."Locations" IS 'A physical location associated with an establishment.';

COMMENT ON COLUMN "public"."Locations".Id IS 'The database Identifier for the location.';

COMMENT ON COLUMN "public"."Locations".EstablishmentId IS 'The database Identifier for the establishment that owns this location.';

COMMENT ON COLUMN "public"."Locations".Name IS 'The Name of the location';

COMMENT ON COLUMN "public"."Locations".Address IS 'The street Address of the physical location.';

COMMENT ON COLUMN "public"."Locations".City IS 'The City that the location is in.';

COMMENT ON COLUMN "public"."Locations".Address_state IS 'The state that the location exists in.';

COMMENT ON COLUMN "public"."Locations".PostalCode IS 'The ZIP.';

COMMENT ON TABLE "public"."People" IS 'The People that use the application.';

COMMENT ON COLUMN "public"."People".Id IS 'The database Identifier for the person.';

COMMENT ON COLUMN "public"."People".Name IS 'The person''s Name.';

COMMENT ON COLUMN "public"."People".Email IS 'The person''s Email.';

COMMENT ON TABLE "public"."Accounts" IS 'The Accounts for administrators to access the system.';

COMMENT ON COLUMN "public"."Accounts".PersonId IS 'The database Identifier for the credentials.';

COMMENT ON COLUMN "public"."Accounts".HashedPassword IS 'The cryptographically hashed password for the account.';

COMMENT ON COLUMN "public"."Accounts".Internal IS 'Indicates that the account is for a person Internal to the organization.';

COMMENT ON TABLE "public"."CampaignLocations" IS 'The establishment Locations that participate in a campaign.';

COMMENT ON COLUMN "public"."CampaignLocations".Id IS 'The database Identifier for the location''s participation in a campaign.';

COMMENT ON COLUMN "public"."CampaignLocations".LocationId IS 'The database Identifier for the location participating in the campaign.';

COMMENT ON COLUMN "public"."CampaignLocations".CampaignId IS 'The database Identifier for the campaign that the location is participating in.';

COMMENT ON COLUMN "public"."CampaignLocations".Notes IS 'Notes about the location participating in the campaign.';

COMMENT ON TABLE "public"."Contacts" IS 'The Contacts for an establishment.';

COMMENT ON COLUMN "public"."Contacts".PersonId IS 'The database Identifier for the specific contact from the Accounts table.';

COMMENT ON COLUMN "public"."Contacts".EstablishmentId IS 'The database Identifier of the establishment that the contact works for.';

COMMENT ON COLUMN "public"."Contacts".ContactTypeId IS 'The type of contact for the establishment.';

COMMENT ON COLUMN "public"."Contacts".Phone IS 'The Phone number for the contact.';

COMMENT ON COLUMN "public"."Contacts".Active IS 'Whether or not the contact is still Active.';

COMMENT ON INDEX "public".Idx_Sessions_Jti_RefreshToken IS 'Index on Jti and RefreshToken for quick lookups during the refresh workflow.';

COMMENT ON CONSTRAINT unq_Sessions_Jti ON "public"."Sessions" IS 'Each Jti should be unique.';

COMMENT ON TABLE "public"."Sessions" IS 'The ongoing Sessions for Accounts.';

COMMENT ON COLUMN "public"."Sessions".Id IS 'The database Identifier for the session.';

COMMENT ON COLUMN "public"."Sessions".AccountId IS 'The database Identifier for the account that is in this session.';

COMMENT ON COLUMN "public"."Sessions".Jti IS 'The unique Identifier for the JWT associated with this session.';

COMMENT ON COLUMN "public"."Sessions".RefreshToken IS 'The token that can be used to refresh this specific session.';

COMMENT ON COLUMN "public"."Sessions".Active IS 'A flag to indicate which session is Active for an account.';

COMMENT ON COLUMN "public"."Sessions".Created IS 'The timestamp when this was Created';

COMMENT ON TABLE "public"."CampaignLocationEvents" IS 'The event types that a location participating in a campaign will host.';

COMMENT ON COLUMN "public"."CampaignLocationEvents".Id IS 'The database Identifier for the location''s willingness to host a specific event type.';

COMMENT ON COLUMN "public"."CampaignLocationEvents".CampaignLocationId IS 'The database Identifier of the location''s participation in a campaign.';

COMMENT ON COLUMN "public"."CampaignLocationEvents".ParticipantCount IS 'The number of participants that the location hosted for the specific type of event over the course of the campaign.';

COMMENT ON COLUMN "public"."CampaignLocationEvents".Entered IS 'The timestamp at which point the count was Entered.';

COMMENT ON COLUMN "public"."CampaignLocationEvents".Invoiced IS 'The date on which the invoice was sent to the location.';

COMMENT ON COLUMN "public"."CampaignLocationEvents".Paid IS 'The date on which the location Paid the invoice.';

COMMENT ON COLUMN "public"."CampaignLocationEvents".EventTypeId IS 'The specific event type that the count applies to.';

COMMENT ON COLUMN "public"."CampaignLocationEvents".Notes IS 'Notes about the recorded count and the location''s work with the specific event type.';
