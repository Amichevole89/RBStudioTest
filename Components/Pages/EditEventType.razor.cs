using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Radzen;
using Radzen.Blazor;

namespace RadzenTestProj.Components.Pages
{
    public partial class EditEventType
    {
        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected DialogService DialogService { get; set; }

        [Inject]
        protected TooltipService TooltipService { get; set; }

        [Inject]
        protected ContextMenuService ContextMenuService { get; set; }

        [Inject]
        protected NotificationService NotificationService { get; set; }
        [Inject]
        public RbsDbContextService RbsDbContextService { get; set; }

        [Parameter]
        public int Id { get; set; }

        protected override async Task OnInitializedAsync()
        {
            eventType = await RbsDbContextService.GetEventTypeById(Id);

            campaignsForCampaignId = await RbsDbContextService.GetCampaigns();
        }
        protected bool errorVisible;
        protected RadzenTestProj.Models.RbsDbContext.EventType eventType;

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.Campaign> campaignsForCampaignId;

        protected async Task FormSubmit()
        {
            try
            {
                await RbsDbContextService.UpdateEventType(Id, eventType);
                DialogService.Close(eventType);
            }
            catch (Exception ex)
            {
                hasChanges = ex is Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException;
                canEdit = !(ex is Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException);
                errorVisible = true;
            }
        }

        protected async Task CancelButtonClick(MouseEventArgs args)
        {
            DialogService.Close(null);
        }


        protected bool hasChanges = false;
        protected bool canEdit = true;

        [Inject]
        protected SecurityService Security { get; set; }


        protected async Task ReloadButtonClick(MouseEventArgs args)
        {
           RbsDbContextService.Reset();
            hasChanges = false;
            canEdit = true;

            eventType = await RbsDbContextService.GetEventTypeById(Id);
        }
    }
}