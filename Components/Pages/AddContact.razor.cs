using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Radzen;
using Radzen.Blazor;

namespace RadzenTestProj.Components.Pages
{
    public partial class AddContact
    {
        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected DialogService DialogService { get; set; }

        [Inject]
        protected TooltipService TooltipService { get; set; }

        [Inject]
        protected ContextMenuService ContextMenuService { get; set; }

        [Inject]
        protected NotificationService NotificationService { get; set; }
        [Inject]
        public RbsDbContextService RbsDbContextService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            contact = new RadzenTestProj.Models.RbsDbContext.Contact();

            peopleForPersonId = await RbsDbContextService.GetPeople();

            establishmentsForEstablishmentId = await RbsDbContextService.GetEstablishments();

            contactTypesForContactTypeId = await RbsDbContextService.GetContactTypes();
        }
        protected bool errorVisible;
        protected RadzenTestProj.Models.RbsDbContext.Contact contact;

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.Person> peopleForPersonId;

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.Establishment> establishmentsForEstablishmentId;

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.ContactType> contactTypesForContactTypeId;

        protected async Task FormSubmit()
        {
            try
            {
                await RbsDbContextService.CreateContact(contact);
                DialogService.Close(contact);
            }
            catch (Exception ex)
            {
                hasChanges = ex is Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException;
                canEdit = !(ex is Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException);
                errorVisible = true;
            }
        }

        protected async Task CancelButtonClick(MouseEventArgs args)
        {
            DialogService.Close(null);
        }


        protected bool hasChanges = false;
        protected bool canEdit = true;

        [Inject]
        protected SecurityService Security { get; set; }
    }
}