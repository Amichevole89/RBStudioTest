using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Radzen;
using Radzen.Blazor;

namespace RadzenTestProj.Components.Pages
{
    public partial class Accounts
    {
        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected DialogService DialogService { get; set; }

        [Inject]
        protected TooltipService TooltipService { get; set; }

        [Inject]
        protected ContextMenuService ContextMenuService { get; set; }

        [Inject]
        protected NotificationService NotificationService { get; set; }

        [Inject]
        public RbsDbContextService RbsDbContextService { get; set; }

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.Account> accounts;

        protected RadzenDataGrid<RadzenTestProj.Models.RbsDbContext.Account> grid0;

        protected string search = "";

        [Inject]
        protected SecurityService Security { get; set; }

        protected async Task Search(ChangeEventArgs args)
        {
            search = $"{args.Value}";

            await grid0.GoToPage(0);

            accounts = await RbsDbContextService.GetAccounts(new Query { Filter = $@"i => i.HashedPassword.Contains(@0)", FilterParameters = new object[] { search }, Expand = "Person" });
        }
        protected override async Task OnInitializedAsync()
        {
            accounts = await RbsDbContextService.GetAccounts(new Query { Filter = $@"i => i.HashedPassword.Contains(@0)", FilterParameters = new object[] { search }, Expand = "Person" });
        }

        protected async Task AddButtonClick(MouseEventArgs args)
        {
            await DialogService.OpenAsync<AddAccount>("Add Account", null);
            await grid0.Reload();
        }

        protected async Task EditRow(DataGridRowMouseEventArgs<RadzenTestProj.Models.RbsDbContext.Account> args)
        {
            await DialogService.OpenAsync<EditAccount>("Edit Account", new Dictionary<string, object> { {"PersonId", args.Data.PersonId} });
        }

        protected async Task GridDeleteButtonClick(MouseEventArgs args, RadzenTestProj.Models.RbsDbContext.Account account)
        {
            try
            {
                if (await DialogService.Confirm("Are you sure you want to delete this record?") == true)
                {
                    var deleteResult = await RbsDbContextService.DeleteAccount(account.PersonId);

                    if (deleteResult != null)
                    {
                        await grid0.Reload();
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationService.Notify(new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = $"Error",
                    Detail = $"Unable to delete Account"
                });
            }
        }
    }
}