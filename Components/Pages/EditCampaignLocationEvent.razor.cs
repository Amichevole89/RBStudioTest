using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Radzen;
using Radzen.Blazor;

namespace RadzenTestProj.Components.Pages
{
    public partial class EditCampaignLocationEvent
    {
        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected DialogService DialogService { get; set; }

        [Inject]
        protected TooltipService TooltipService { get; set; }

        [Inject]
        protected ContextMenuService ContextMenuService { get; set; }

        [Inject]
        protected NotificationService NotificationService { get; set; }
        [Inject]
        public RbsDbContextService RbsDbContextService { get; set; }

        [Parameter]
        public int Id { get; set; }

        protected override async Task OnInitializedAsync()
        {
            campaignLocationEvent = await RbsDbContextService.GetCampaignLocationEventById(Id);

            campaignLocationsForCampaignLocationId = await RbsDbContextService.GetCampaignLocations();

            eventTypesForEventTypeId = await RbsDbContextService.GetEventTypes();
        }
        protected bool errorVisible;
        protected RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent campaignLocationEvent;

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.CampaignLocation> campaignLocationsForCampaignLocationId;

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.EventType> eventTypesForEventTypeId;

        protected async Task FormSubmit()
        {
            try
            {
                await RbsDbContextService.UpdateCampaignLocationEvent(Id, campaignLocationEvent);
                DialogService.Close(campaignLocationEvent);
            }
            catch (Exception ex)
            {
                hasChanges = ex is Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException;
                canEdit = !(ex is Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException);
                errorVisible = true;
            }
        }

        protected async Task CancelButtonClick(MouseEventArgs args)
        {
            DialogService.Close(null);
        }


        protected bool hasChanges = false;
        protected bool canEdit = true;

        [Inject]
        protected SecurityService Security { get; set; }


        protected async Task ReloadButtonClick(MouseEventArgs args)
        {
           RbsDbContextService.Reset();
            hasChanges = false;
            canEdit = true;

            campaignLocationEvent = await RbsDbContextService.GetCampaignLocationEventById(Id);
        }
    }
}