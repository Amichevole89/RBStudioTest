using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Radzen;
using Radzen.Blazor;

namespace RadzenTestProj.Components.Pages
{
    public partial class CampaignLocationEvents
    {
        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected DialogService DialogService { get; set; }

        [Inject]
        protected TooltipService TooltipService { get; set; }

        [Inject]
        protected ContextMenuService ContextMenuService { get; set; }

        [Inject]
        protected NotificationService NotificationService { get; set; }

        [Inject]
        public RbsDbContextService RbsDbContextService { get; set; }

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> campaignLocationEvents;

        protected RadzenDataGrid<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> grid0;

        protected string search = "";

        [Inject]
        protected SecurityService Security { get; set; }

        protected async Task Search(ChangeEventArgs args)
        {
            search = $"{args.Value}";

            await grid0.GoToPage(0);

            campaignLocationEvents = await RbsDbContextService.GetCampaignLocationEvents(new Query { Filter = $@"i => i.Notes.Contains(@0)", FilterParameters = new object[] { search }, Expand = "CampaignLocation,EventType" });
        }
        protected override async Task OnInitializedAsync()
        {
            campaignLocationEvents = await RbsDbContextService.GetCampaignLocationEvents(new Query { Filter = $@"i => i.Notes.Contains(@0)", FilterParameters = new object[] { search }, Expand = "CampaignLocation,EventType" });
        }

        protected async Task AddButtonClick(MouseEventArgs args)
        {
            await DialogService.OpenAsync<AddCampaignLocationEvent>("Add CampaignLocationEvent", null);
            await grid0.Reload();
        }

        protected async Task EditRow(DataGridRowMouseEventArgs<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> args)
        {
            await DialogService.OpenAsync<EditCampaignLocationEvent>("Edit CampaignLocationEvent", new Dictionary<string, object> { {"Id", args.Data.Id} });
        }

        protected async Task GridDeleteButtonClick(MouseEventArgs args, RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent campaignLocationEvent)
        {
            try
            {
                if (await DialogService.Confirm("Are you sure you want to delete this record?") == true)
                {
                    var deleteResult = await RbsDbContextService.DeleteCampaignLocationEvent(campaignLocationEvent.Id);

                    if (deleteResult != null)
                    {
                        await grid0.Reload();
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationService.Notify(new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = $"Error",
                    Detail = $"Unable to delete CampaignLocationEvent"
                });
            }
        }
    }
}