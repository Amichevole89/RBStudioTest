using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Radzen;
using Radzen.Blazor;

namespace RadzenTestProj.Components.Pages
{
    public partial class CampaignLocations
    {
        [Inject]
        protected IJSRuntime JSRuntime { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject]
        protected DialogService DialogService { get; set; }

        [Inject]
        protected TooltipService TooltipService { get; set; }

        [Inject]
        protected ContextMenuService ContextMenuService { get; set; }

        [Inject]
        protected NotificationService NotificationService { get; set; }

        [Inject]
        public RbsDbContextService RbsDbContextService { get; set; }

        protected IEnumerable<RadzenTestProj.Models.RbsDbContext.CampaignLocation> campaignLocations;

        protected RadzenDataGrid<RadzenTestProj.Models.RbsDbContext.CampaignLocation> grid0;

        protected string search = "";

        [Inject]
        protected SecurityService Security { get; set; }

        protected async Task Search(ChangeEventArgs args)
        {
            search = $"{args.Value}";

            await grid0.GoToPage(0);

            campaignLocations = await RbsDbContextService.GetCampaignLocations(new Query { Filter = $@"i => i.Notes.Contains(@0)", FilterParameters = new object[] { search }, Expand = "Location,Campaign" });
        }
        protected override async Task OnInitializedAsync()
        {
            campaignLocations = await RbsDbContextService.GetCampaignLocations(new Query { Filter = $@"i => i.Notes.Contains(@0)", FilterParameters = new object[] { search }, Expand = "Location,Campaign" });
        }

        protected async Task AddButtonClick(MouseEventArgs args)
        {
            await DialogService.OpenAsync<AddCampaignLocation>("Add CampaignLocation", null);
            await grid0.Reload();
        }

        protected async Task EditRow(DataGridRowMouseEventArgs<RadzenTestProj.Models.RbsDbContext.CampaignLocation> args)
        {
            await DialogService.OpenAsync<EditCampaignLocation>("Edit CampaignLocation", new Dictionary<string, object> { {"Id", args.Data.Id} });
        }

        protected async Task GridDeleteButtonClick(MouseEventArgs args, RadzenTestProj.Models.RbsDbContext.CampaignLocation campaignLocation)
        {
            try
            {
                if (await DialogService.Confirm("Are you sure you want to delete this record?") == true)
                {
                    var deleteResult = await RbsDbContextService.DeleteCampaignLocation(campaignLocation.Id);

                    if (deleteResult != null)
                    {
                        await grid0.Reload();
                    }
                }
            }
            catch (Exception ex)
            {
                NotificationService.Notify(new NotificationMessage
                {
                    Severity = NotificationSeverity.Error,
                    Summary = $"Error",
                    Detail = $"Unable to delete CampaignLocation"
                });
            }
        }
    }
}