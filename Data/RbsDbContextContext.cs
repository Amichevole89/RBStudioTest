using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using RadzenTestProj.Models.RbsDbContext;

namespace RadzenTestProj.Data
{
    public partial class RbsDbContextContext : DbContext
    {
        public RbsDbContextContext()
        {
        }

        public RbsDbContextContext(DbContextOptions<RbsDbContextContext> options) : base(options)
        {
        }

        partial void OnModelBuilding(ModelBuilder builder);

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Account>()
              .HasOne(i => i.Person)
              .WithMany(i => i.Accounts)
              .HasForeignKey(i => i.PersonId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent>()
              .HasOne(i => i.CampaignLocation)
              .WithMany(i => i.CampaignLocationEvents)
              .HasForeignKey(i => i.CampaignLocationId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent>()
              .HasOne(i => i.EventType)
              .WithMany(i => i.CampaignLocationEvents)
              .HasForeignKey(i => i.EventTypeId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.CampaignLocation>()
              .HasOne(i => i.Campaign)
              .WithMany(i => i.CampaignLocations)
              .HasForeignKey(i => i.CampaignId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.CampaignLocation>()
              .HasOne(i => i.Location)
              .WithMany(i => i.CampaignLocations)
              .HasForeignKey(i => i.LocationId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Contact>()
              .HasOne(i => i.ContactType)
              .WithMany(i => i.Contacts)
              .HasForeignKey(i => i.ContactTypeId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Contact>()
              .HasOne(i => i.Establishment)
              .WithMany(i => i.Contacts)
              .HasForeignKey(i => i.EstablishmentId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Contact>()
              .HasOne(i => i.Person)
              .WithMany(i => i.Contacts)
              .HasForeignKey(i => i.PersonId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.EventType>()
              .HasOne(i => i.Campaign)
              .WithMany(i => i.EventTypes)
              .HasForeignKey(i => i.CampaignId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Location>()
              .HasOne(i => i.Establishment)
              .WithMany(i => i.Locations)
              .HasForeignKey(i => i.EstablishmentId)
              .HasPrincipalKey(i => i.Id);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Session>()
              .HasOne(i => i.Account)
              .WithMany(i => i.Sessions)
              .HasForeignKey(i => i.AccountId)
              .HasPrincipalKey(i => i.PersonId);

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Account>()
              .Property(p => p.Active)
              .HasDefaultValueSql(@"true");

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Contact>()
              .Property(p => p.Active)
              .HasDefaultValueSql(@"true");

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Location>()
              .Property(p => p.City)
              .HasDefaultValueSql(@"'Houston'::character varying");

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Location>()
              .Property(p => p.Address_state)
              .HasDefaultValueSql(@"'TX'::bpchar");

            builder.Entity<RadzenTestProj.Models.RbsDbContext.Session>()
              .Property(p => p.Created)
              .HasDefaultValueSql(@"CURRENT_TIMESTAMP");
            this.OnModelBuilding(builder);
        }

        public DbSet<RadzenTestProj.Models.RbsDbContext.Account> Accounts { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> CampaignLocationEvents { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.CampaignLocation> CampaignLocations { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.Campaign> Campaigns { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.Contact> Contacts { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.ContactType> ContactTypes { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.Establishment> Establishments { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.EventType> EventTypes { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.Location> Locations { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.Person> People { get; set; }

        public DbSet<RadzenTestProj.Models.RbsDbContext.Session> Sessions { get; set; }

        protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.Conventions.Add(_ => new BlankTriggerAddingConvention());
        }
    
    }
}