using System;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using Radzen;

using RadzenTestProj.Data;

namespace RadzenTestProj
{
    public partial class RbsDbContextService
    {
        RbsDbContextContext Context
        {
           get
           {
             return this.context;
           }
        }

        private readonly RbsDbContextContext context;
        private readonly NavigationManager navigationManager;

        public RbsDbContextService(RbsDbContextContext context, NavigationManager navigationManager)
        {
            this.context = context;
            this.navigationManager = navigationManager;
        }

        public void Reset() => Context.ChangeTracker.Entries().Where(e => e.Entity != null).ToList().ForEach(e => e.State = EntityState.Detached);

        public void ApplyQuery<T>(ref IQueryable<T> items, Query query = null)
        {
            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Filter))
                {
                    if (query.FilterParameters != null)
                    {
                        items = items.Where(query.Filter, query.FilterParameters);
                    }
                    else
                    {
                        items = items.Where(query.Filter);
                    }
                }

                if (!string.IsNullOrEmpty(query.OrderBy))
                {
                    items = items.OrderBy(query.OrderBy);
                }

                if (query.Skip.HasValue)
                {
                    items = items.Skip(query.Skip.Value);
                }

                if (query.Top.HasValue)
                {
                    items = items.Take(query.Top.Value);
                }
            }
        }


        public async Task ExportAccountsToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/accounts/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/accounts/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportAccountsToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/accounts/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/accounts/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnAccountsRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Account> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.Account>> GetAccounts(Query query = null)
        {
            var items = Context.Accounts.AsQueryable();

            items = items.Include(i => i.Person);

            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnAccountsRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnAccountGet(RadzenTestProj.Models.RbsDbContext.Account item);
        partial void OnGetAccountByPersonId(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Account> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.Account> GetAccountByPersonId(Guid personid)
        {
            var items = Context.Accounts
                              .AsNoTracking()
                              .Where(i => i.PersonId == personid);

            items = items.Include(i => i.Person);
 
            OnGetAccountByPersonId(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnAccountGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnAccountCreated(RadzenTestProj.Models.RbsDbContext.Account item);
        partial void OnAfterAccountCreated(RadzenTestProj.Models.RbsDbContext.Account item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Account> CreateAccount(RadzenTestProj.Models.RbsDbContext.Account account)
        {
            OnAccountCreated(account);

            var existingItem = Context.Accounts
                              .Where(i => i.PersonId == account.PersonId)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.Accounts.Add(account);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(account).State = EntityState.Detached;
                throw;
            }

            OnAfterAccountCreated(account);

            return account;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.Account> CancelAccountChanges(RadzenTestProj.Models.RbsDbContext.Account item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnAccountUpdated(RadzenTestProj.Models.RbsDbContext.Account item);
        partial void OnAfterAccountUpdated(RadzenTestProj.Models.RbsDbContext.Account item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Account> UpdateAccount(Guid personid, RadzenTestProj.Models.RbsDbContext.Account account)
        {
            OnAccountUpdated(account);

            var itemToUpdate = Context.Accounts
                              .Where(i => i.PersonId == account.PersonId)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(account);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterAccountUpdated(account);

            return account;
        }

        partial void OnAccountDeleted(RadzenTestProj.Models.RbsDbContext.Account item);
        partial void OnAfterAccountDeleted(RadzenTestProj.Models.RbsDbContext.Account item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Account> DeleteAccount(Guid personid)
        {
            var itemToDelete = Context.Accounts
                              .Where(i => i.PersonId == personid)
                              .Include(i => i.Sessions)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnAccountDeleted(itemToDelete);


            Context.Accounts.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterAccountDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportCampaignLocationEventsToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/campaignlocationevents/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/campaignlocationevents/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportCampaignLocationEventsToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/campaignlocationevents/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/campaignlocationevents/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnCampaignLocationEventsRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent>> GetCampaignLocationEvents(Query query = null)
        {
            var items = Context.CampaignLocationEvents.AsQueryable();

            items = items.Include(i => i.CampaignLocation);
            items = items.Include(i => i.EventType);

            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnCampaignLocationEventsRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnCampaignLocationEventGet(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent item);
        partial void OnGetCampaignLocationEventById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> GetCampaignLocationEventById(int id)
        {
            var items = Context.CampaignLocationEvents
                              .AsNoTracking()
                              .Where(i => i.Id == id);

            items = items.Include(i => i.CampaignLocation);
            items = items.Include(i => i.EventType);
 
            OnGetCampaignLocationEventById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnCampaignLocationEventGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnCampaignLocationEventCreated(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent item);
        partial void OnAfterCampaignLocationEventCreated(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent item);

        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> CreateCampaignLocationEvent(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent campaignlocationevent)
        {
            OnCampaignLocationEventCreated(campaignlocationevent);

            var existingItem = Context.CampaignLocationEvents
                              .Where(i => i.Id == campaignlocationevent.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.CampaignLocationEvents.Add(campaignlocationevent);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(campaignlocationevent).State = EntityState.Detached;
                throw;
            }

            OnAfterCampaignLocationEventCreated(campaignlocationevent);

            return campaignlocationevent;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> CancelCampaignLocationEventChanges(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnCampaignLocationEventUpdated(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent item);
        partial void OnAfterCampaignLocationEventUpdated(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent item);

        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> UpdateCampaignLocationEvent(int id, RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent campaignlocationevent)
        {
            OnCampaignLocationEventUpdated(campaignlocationevent);

            var itemToUpdate = Context.CampaignLocationEvents
                              .Where(i => i.Id == campaignlocationevent.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(campaignlocationevent);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterCampaignLocationEventUpdated(campaignlocationevent);

            return campaignlocationevent;
        }

        partial void OnCampaignLocationEventDeleted(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent item);
        partial void OnAfterCampaignLocationEventDeleted(RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent item);

        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocationEvent> DeleteCampaignLocationEvent(int id)
        {
            var itemToDelete = Context.CampaignLocationEvents
                              .Where(i => i.Id == id)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnCampaignLocationEventDeleted(itemToDelete);


            Context.CampaignLocationEvents.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterCampaignLocationEventDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportCampaignLocationsToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/campaignlocations/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/campaignlocations/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportCampaignLocationsToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/campaignlocations/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/campaignlocations/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnCampaignLocationsRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.CampaignLocation> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.CampaignLocation>> GetCampaignLocations(Query query = null)
        {
            var items = Context.CampaignLocations.AsQueryable();

            items = items.Include(i => i.Campaign);
            items = items.Include(i => i.Location);

            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnCampaignLocationsRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnCampaignLocationGet(RadzenTestProj.Models.RbsDbContext.CampaignLocation item);
        partial void OnGetCampaignLocationById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.CampaignLocation> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocation> GetCampaignLocationById(int id)
        {
            var items = Context.CampaignLocations
                              .AsNoTracking()
                              .Where(i => i.Id == id);

            items = items.Include(i => i.Campaign);
            items = items.Include(i => i.Location);
 
            OnGetCampaignLocationById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnCampaignLocationGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnCampaignLocationCreated(RadzenTestProj.Models.RbsDbContext.CampaignLocation item);
        partial void OnAfterCampaignLocationCreated(RadzenTestProj.Models.RbsDbContext.CampaignLocation item);

        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocation> CreateCampaignLocation(RadzenTestProj.Models.RbsDbContext.CampaignLocation campaignlocation)
        {
            OnCampaignLocationCreated(campaignlocation);

            var existingItem = Context.CampaignLocations
                              .Where(i => i.Id == campaignlocation.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.CampaignLocations.Add(campaignlocation);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(campaignlocation).State = EntityState.Detached;
                throw;
            }

            OnAfterCampaignLocationCreated(campaignlocation);

            return campaignlocation;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocation> CancelCampaignLocationChanges(RadzenTestProj.Models.RbsDbContext.CampaignLocation item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnCampaignLocationUpdated(RadzenTestProj.Models.RbsDbContext.CampaignLocation item);
        partial void OnAfterCampaignLocationUpdated(RadzenTestProj.Models.RbsDbContext.CampaignLocation item);

        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocation> UpdateCampaignLocation(int id, RadzenTestProj.Models.RbsDbContext.CampaignLocation campaignlocation)
        {
            OnCampaignLocationUpdated(campaignlocation);

            var itemToUpdate = Context.CampaignLocations
                              .Where(i => i.Id == campaignlocation.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(campaignlocation);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterCampaignLocationUpdated(campaignlocation);

            return campaignlocation;
        }

        partial void OnCampaignLocationDeleted(RadzenTestProj.Models.RbsDbContext.CampaignLocation item);
        partial void OnAfterCampaignLocationDeleted(RadzenTestProj.Models.RbsDbContext.CampaignLocation item);

        public async Task<RadzenTestProj.Models.RbsDbContext.CampaignLocation> DeleteCampaignLocation(int id)
        {
            var itemToDelete = Context.CampaignLocations
                              .Where(i => i.Id == id)
                              .Include(i => i.CampaignLocationEvents)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnCampaignLocationDeleted(itemToDelete);


            Context.CampaignLocations.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterCampaignLocationDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportCampaignsToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/campaigns/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/campaigns/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportCampaignsToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/campaigns/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/campaigns/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnCampaignsRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Campaign> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.Campaign>> GetCampaigns(Query query = null)
        {
            var items = Context.Campaigns.AsQueryable();


            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnCampaignsRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnCampaignGet(RadzenTestProj.Models.RbsDbContext.Campaign item);
        partial void OnGetCampaignById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Campaign> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.Campaign> GetCampaignById(int id)
        {
            var items = Context.Campaigns
                              .AsNoTracking()
                              .Where(i => i.Id == id);

 
            OnGetCampaignById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnCampaignGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnCampaignCreated(RadzenTestProj.Models.RbsDbContext.Campaign item);
        partial void OnAfterCampaignCreated(RadzenTestProj.Models.RbsDbContext.Campaign item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Campaign> CreateCampaign(RadzenTestProj.Models.RbsDbContext.Campaign campaign)
        {
            OnCampaignCreated(campaign);

            var existingItem = Context.Campaigns
                              .Where(i => i.Id == campaign.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.Campaigns.Add(campaign);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(campaign).State = EntityState.Detached;
                throw;
            }

            OnAfterCampaignCreated(campaign);

            return campaign;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.Campaign> CancelCampaignChanges(RadzenTestProj.Models.RbsDbContext.Campaign item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnCampaignUpdated(RadzenTestProj.Models.RbsDbContext.Campaign item);
        partial void OnAfterCampaignUpdated(RadzenTestProj.Models.RbsDbContext.Campaign item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Campaign> UpdateCampaign(int id, RadzenTestProj.Models.RbsDbContext.Campaign campaign)
        {
            OnCampaignUpdated(campaign);

            var itemToUpdate = Context.Campaigns
                              .Where(i => i.Id == campaign.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(campaign);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterCampaignUpdated(campaign);

            return campaign;
        }

        partial void OnCampaignDeleted(RadzenTestProj.Models.RbsDbContext.Campaign item);
        partial void OnAfterCampaignDeleted(RadzenTestProj.Models.RbsDbContext.Campaign item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Campaign> DeleteCampaign(int id)
        {
            var itemToDelete = Context.Campaigns
                              .Where(i => i.Id == id)
                              .Include(i => i.CampaignLocations)
                              .Include(i => i.EventTypes)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnCampaignDeleted(itemToDelete);


            Context.Campaigns.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterCampaignDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportContactsToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/contacts/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/contacts/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportContactsToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/contacts/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/contacts/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnContactsRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Contact> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.Contact>> GetContacts(Query query = null)
        {
            var items = Context.Contacts.AsQueryable();

            items = items.Include(i => i.ContactType);
            items = items.Include(i => i.Establishment);
            items = items.Include(i => i.Person);

            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnContactsRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnContactGet(RadzenTestProj.Models.RbsDbContext.Contact item);
        partial void OnGetContactByPersonId(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Contact> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.Contact> GetContactByPersonId(Guid personid)
        {
            var items = Context.Contacts
                              .AsNoTracking()
                              .Where(i => i.PersonId == personid);

            items = items.Include(i => i.ContactType);
            items = items.Include(i => i.Establishment);
            items = items.Include(i => i.Person);
 
            OnGetContactByPersonId(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnContactGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnContactCreated(RadzenTestProj.Models.RbsDbContext.Contact item);
        partial void OnAfterContactCreated(RadzenTestProj.Models.RbsDbContext.Contact item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Contact> CreateContact(RadzenTestProj.Models.RbsDbContext.Contact contact)
        {
            OnContactCreated(contact);

            var existingItem = Context.Contacts
                              .Where(i => i.PersonId == contact.PersonId)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.Contacts.Add(contact);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(contact).State = EntityState.Detached;
                throw;
            }

            OnAfterContactCreated(contact);

            return contact;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.Contact> CancelContactChanges(RadzenTestProj.Models.RbsDbContext.Contact item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnContactUpdated(RadzenTestProj.Models.RbsDbContext.Contact item);
        partial void OnAfterContactUpdated(RadzenTestProj.Models.RbsDbContext.Contact item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Contact> UpdateContact(Guid personid, RadzenTestProj.Models.RbsDbContext.Contact contact)
        {
            OnContactUpdated(contact);

            var itemToUpdate = Context.Contacts
                              .Where(i => i.PersonId == contact.PersonId)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(contact);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterContactUpdated(contact);

            return contact;
        }

        partial void OnContactDeleted(RadzenTestProj.Models.RbsDbContext.Contact item);
        partial void OnAfterContactDeleted(RadzenTestProj.Models.RbsDbContext.Contact item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Contact> DeleteContact(Guid personid)
        {
            var itemToDelete = Context.Contacts
                              .Where(i => i.PersonId == personid)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnContactDeleted(itemToDelete);


            Context.Contacts.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterContactDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportContactTypesToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/contacttypes/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/contacttypes/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportContactTypesToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/contacttypes/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/contacttypes/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnContactTypesRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.ContactType> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.ContactType>> GetContactTypes(Query query = null)
        {
            var items = Context.ContactTypes.AsQueryable();


            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnContactTypesRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnContactTypeGet(RadzenTestProj.Models.RbsDbContext.ContactType item);
        partial void OnGetContactTypeById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.ContactType> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.ContactType> GetContactTypeById(int id)
        {
            var items = Context.ContactTypes
                              .AsNoTracking()
                              .Where(i => i.Id == id);

 
            OnGetContactTypeById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnContactTypeGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnContactTypeCreated(RadzenTestProj.Models.RbsDbContext.ContactType item);
        partial void OnAfterContactTypeCreated(RadzenTestProj.Models.RbsDbContext.ContactType item);

        public async Task<RadzenTestProj.Models.RbsDbContext.ContactType> CreateContactType(RadzenTestProj.Models.RbsDbContext.ContactType contacttype)
        {
            OnContactTypeCreated(contacttype);

            var existingItem = Context.ContactTypes
                              .Where(i => i.Id == contacttype.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.ContactTypes.Add(contacttype);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(contacttype).State = EntityState.Detached;
                throw;
            }

            OnAfterContactTypeCreated(contacttype);

            return contacttype;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.ContactType> CancelContactTypeChanges(RadzenTestProj.Models.RbsDbContext.ContactType item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnContactTypeUpdated(RadzenTestProj.Models.RbsDbContext.ContactType item);
        partial void OnAfterContactTypeUpdated(RadzenTestProj.Models.RbsDbContext.ContactType item);

        public async Task<RadzenTestProj.Models.RbsDbContext.ContactType> UpdateContactType(int id, RadzenTestProj.Models.RbsDbContext.ContactType contacttype)
        {
            OnContactTypeUpdated(contacttype);

            var itemToUpdate = Context.ContactTypes
                              .Where(i => i.Id == contacttype.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(contacttype);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterContactTypeUpdated(contacttype);

            return contacttype;
        }

        partial void OnContactTypeDeleted(RadzenTestProj.Models.RbsDbContext.ContactType item);
        partial void OnAfterContactTypeDeleted(RadzenTestProj.Models.RbsDbContext.ContactType item);

        public async Task<RadzenTestProj.Models.RbsDbContext.ContactType> DeleteContactType(int id)
        {
            var itemToDelete = Context.ContactTypes
                              .Where(i => i.Id == id)
                              .Include(i => i.Contacts)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnContactTypeDeleted(itemToDelete);


            Context.ContactTypes.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterContactTypeDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportEstablishmentsToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/establishments/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/establishments/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportEstablishmentsToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/establishments/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/establishments/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnEstablishmentsRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Establishment> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.Establishment>> GetEstablishments(Query query = null)
        {
            var items = Context.Establishments.AsQueryable();


            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnEstablishmentsRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnEstablishmentGet(RadzenTestProj.Models.RbsDbContext.Establishment item);
        partial void OnGetEstablishmentById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Establishment> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.Establishment> GetEstablishmentById(int id)
        {
            var items = Context.Establishments
                              .AsNoTracking()
                              .Where(i => i.Id == id);

 
            OnGetEstablishmentById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnEstablishmentGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnEstablishmentCreated(RadzenTestProj.Models.RbsDbContext.Establishment item);
        partial void OnAfterEstablishmentCreated(RadzenTestProj.Models.RbsDbContext.Establishment item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Establishment> CreateEstablishment(RadzenTestProj.Models.RbsDbContext.Establishment establishment)
        {
            OnEstablishmentCreated(establishment);

            var existingItem = Context.Establishments
                              .Where(i => i.Id == establishment.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.Establishments.Add(establishment);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(establishment).State = EntityState.Detached;
                throw;
            }

            OnAfterEstablishmentCreated(establishment);

            return establishment;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.Establishment> CancelEstablishmentChanges(RadzenTestProj.Models.RbsDbContext.Establishment item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnEstablishmentUpdated(RadzenTestProj.Models.RbsDbContext.Establishment item);
        partial void OnAfterEstablishmentUpdated(RadzenTestProj.Models.RbsDbContext.Establishment item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Establishment> UpdateEstablishment(int id, RadzenTestProj.Models.RbsDbContext.Establishment establishment)
        {
            OnEstablishmentUpdated(establishment);

            var itemToUpdate = Context.Establishments
                              .Where(i => i.Id == establishment.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(establishment);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterEstablishmentUpdated(establishment);

            return establishment;
        }

        partial void OnEstablishmentDeleted(RadzenTestProj.Models.RbsDbContext.Establishment item);
        partial void OnAfterEstablishmentDeleted(RadzenTestProj.Models.RbsDbContext.Establishment item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Establishment> DeleteEstablishment(int id)
        {
            var itemToDelete = Context.Establishments
                              .Where(i => i.Id == id)
                              .Include(i => i.Contacts)
                              .Include(i => i.Locations)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnEstablishmentDeleted(itemToDelete);


            Context.Establishments.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterEstablishmentDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportEventTypesToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/eventtypes/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/eventtypes/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportEventTypesToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/eventtypes/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/eventtypes/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnEventTypesRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.EventType> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.EventType>> GetEventTypes(Query query = null)
        {
            var items = Context.EventTypes.AsQueryable();

            items = items.Include(i => i.Campaign);

            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnEventTypesRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnEventTypeGet(RadzenTestProj.Models.RbsDbContext.EventType item);
        partial void OnGetEventTypeById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.EventType> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.EventType> GetEventTypeById(int id)
        {
            var items = Context.EventTypes
                              .AsNoTracking()
                              .Where(i => i.Id == id);

            items = items.Include(i => i.Campaign);
 
            OnGetEventTypeById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnEventTypeGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnEventTypeCreated(RadzenTestProj.Models.RbsDbContext.EventType item);
        partial void OnAfterEventTypeCreated(RadzenTestProj.Models.RbsDbContext.EventType item);

        public async Task<RadzenTestProj.Models.RbsDbContext.EventType> CreateEventType(RadzenTestProj.Models.RbsDbContext.EventType eventtype)
        {
            OnEventTypeCreated(eventtype);

            var existingItem = Context.EventTypes
                              .Where(i => i.Id == eventtype.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.EventTypes.Add(eventtype);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(eventtype).State = EntityState.Detached;
                throw;
            }

            OnAfterEventTypeCreated(eventtype);

            return eventtype;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.EventType> CancelEventTypeChanges(RadzenTestProj.Models.RbsDbContext.EventType item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnEventTypeUpdated(RadzenTestProj.Models.RbsDbContext.EventType item);
        partial void OnAfterEventTypeUpdated(RadzenTestProj.Models.RbsDbContext.EventType item);

        public async Task<RadzenTestProj.Models.RbsDbContext.EventType> UpdateEventType(int id, RadzenTestProj.Models.RbsDbContext.EventType eventtype)
        {
            OnEventTypeUpdated(eventtype);

            var itemToUpdate = Context.EventTypes
                              .Where(i => i.Id == eventtype.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(eventtype);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterEventTypeUpdated(eventtype);

            return eventtype;
        }

        partial void OnEventTypeDeleted(RadzenTestProj.Models.RbsDbContext.EventType item);
        partial void OnAfterEventTypeDeleted(RadzenTestProj.Models.RbsDbContext.EventType item);

        public async Task<RadzenTestProj.Models.RbsDbContext.EventType> DeleteEventType(int id)
        {
            var itemToDelete = Context.EventTypes
                              .Where(i => i.Id == id)
                              .Include(i => i.CampaignLocationEvents)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnEventTypeDeleted(itemToDelete);


            Context.EventTypes.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterEventTypeDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportLocationsToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/locations/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/locations/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportLocationsToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/locations/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/locations/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnLocationsRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Location> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.Location>> GetLocations(Query query = null)
        {
            var items = Context.Locations.AsQueryable();

            items = items.Include(i => i.Establishment);

            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnLocationsRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnLocationGet(RadzenTestProj.Models.RbsDbContext.Location item);
        partial void OnGetLocationById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Location> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.Location> GetLocationById(int id)
        {
            var items = Context.Locations
                              .AsNoTracking()
                              .Where(i => i.Id == id);

            items = items.Include(i => i.Establishment);
 
            OnGetLocationById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnLocationGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnLocationCreated(RadzenTestProj.Models.RbsDbContext.Location item);
        partial void OnAfterLocationCreated(RadzenTestProj.Models.RbsDbContext.Location item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Location> CreateLocation(RadzenTestProj.Models.RbsDbContext.Location location)
        {
            OnLocationCreated(location);

            var existingItem = Context.Locations
                              .Where(i => i.Id == location.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.Locations.Add(location);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(location).State = EntityState.Detached;
                throw;
            }

            OnAfterLocationCreated(location);

            return location;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.Location> CancelLocationChanges(RadzenTestProj.Models.RbsDbContext.Location item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnLocationUpdated(RadzenTestProj.Models.RbsDbContext.Location item);
        partial void OnAfterLocationUpdated(RadzenTestProj.Models.RbsDbContext.Location item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Location> UpdateLocation(int id, RadzenTestProj.Models.RbsDbContext.Location location)
        {
            OnLocationUpdated(location);

            var itemToUpdate = Context.Locations
                              .Where(i => i.Id == location.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(location);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterLocationUpdated(location);

            return location;
        }

        partial void OnLocationDeleted(RadzenTestProj.Models.RbsDbContext.Location item);
        partial void OnAfterLocationDeleted(RadzenTestProj.Models.RbsDbContext.Location item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Location> DeleteLocation(int id)
        {
            var itemToDelete = Context.Locations
                              .Where(i => i.Id == id)
                              .Include(i => i.CampaignLocations)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnLocationDeleted(itemToDelete);


            Context.Locations.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterLocationDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportPeopleToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/people/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/people/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportPeopleToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/people/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/people/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnPeopleRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Person> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.Person>> GetPeople(Query query = null)
        {
            var items = Context.People.AsQueryable();


            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnPeopleRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnPersonGet(RadzenTestProj.Models.RbsDbContext.Person item);
        partial void OnGetPersonById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Person> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.Person> GetPersonById(Guid id)
        {
            var items = Context.People
                              .AsNoTracking()
                              .Where(i => i.Id == id);

 
            OnGetPersonById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnPersonGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnPersonCreated(RadzenTestProj.Models.RbsDbContext.Person item);
        partial void OnAfterPersonCreated(RadzenTestProj.Models.RbsDbContext.Person item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Person> CreatePerson(RadzenTestProj.Models.RbsDbContext.Person person)
        {
            OnPersonCreated(person);

            var existingItem = Context.People
                              .Where(i => i.Id == person.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.People.Add(person);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(person).State = EntityState.Detached;
                throw;
            }

            OnAfterPersonCreated(person);

            return person;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.Person> CancelPersonChanges(RadzenTestProj.Models.RbsDbContext.Person item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnPersonUpdated(RadzenTestProj.Models.RbsDbContext.Person item);
        partial void OnAfterPersonUpdated(RadzenTestProj.Models.RbsDbContext.Person item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Person> UpdatePerson(Guid id, RadzenTestProj.Models.RbsDbContext.Person person)
        {
            OnPersonUpdated(person);

            var itemToUpdate = Context.People
                              .Where(i => i.Id == person.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(person);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterPersonUpdated(person);

            return person;
        }

        partial void OnPersonDeleted(RadzenTestProj.Models.RbsDbContext.Person item);
        partial void OnAfterPersonDeleted(RadzenTestProj.Models.RbsDbContext.Person item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Person> DeletePerson(Guid id)
        {
            var itemToDelete = Context.People
                              .Where(i => i.Id == id)
                              .Include(i => i.Accounts)
                              .Include(i => i.Contacts)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnPersonDeleted(itemToDelete);


            Context.People.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterPersonDeleted(itemToDelete);

            return itemToDelete;
        }
    
        public async Task ExportSessionsToExcel(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/sessions/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/sessions/excel(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        public async Task ExportSessionsToCSV(Query query = null, string fileName = null)
        {
            navigationManager.NavigateTo(query != null ? query.ToUrl($"export/rbsdbcontext/sessions/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')") : $"export/rbsdbcontext/sessions/csv(fileName='{(!string.IsNullOrEmpty(fileName) ? UrlEncoder.Default.Encode(fileName) : "Export")}')", true);
        }

        partial void OnSessionsRead(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Session> items);

        public async Task<IQueryable<RadzenTestProj.Models.RbsDbContext.Session>> GetSessions(Query query = null)
        {
            var items = Context.Sessions.AsQueryable();

            items = items.Include(i => i.Account);

            if (query != null)
            {
                if (!string.IsNullOrEmpty(query.Expand))
                {
                    var propertiesToExpand = query.Expand.Split(',');
                    foreach(var p in propertiesToExpand)
                    {
                        items = items.Include(p.Trim());
                    }
                }

                ApplyQuery(ref items, query);
            }

            OnSessionsRead(ref items);

            return await Task.FromResult(items);
        }

        partial void OnSessionGet(RadzenTestProj.Models.RbsDbContext.Session item);
        partial void OnGetSessionById(ref IQueryable<RadzenTestProj.Models.RbsDbContext.Session> items);


        public async Task<RadzenTestProj.Models.RbsDbContext.Session> GetSessionById(long id)
        {
            var items = Context.Sessions
                              .AsNoTracking()
                              .Where(i => i.Id == id);

            items = items.Include(i => i.Account);
 
            OnGetSessionById(ref items);

            var itemToReturn = items.FirstOrDefault();

            OnSessionGet(itemToReturn);

            return await Task.FromResult(itemToReturn);
        }

        partial void OnSessionCreated(RadzenTestProj.Models.RbsDbContext.Session item);
        partial void OnAfterSessionCreated(RadzenTestProj.Models.RbsDbContext.Session item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Session> CreateSession(RadzenTestProj.Models.RbsDbContext.Session session)
        {
            OnSessionCreated(session);

            var existingItem = Context.Sessions
                              .Where(i => i.Id == session.Id)
                              .FirstOrDefault();

            if (existingItem != null)
            {
               throw new Exception("Item already available");
            }            

            try
            {
                Context.Sessions.Add(session);
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(session).State = EntityState.Detached;
                throw;
            }

            OnAfterSessionCreated(session);

            return session;
        }

        public async Task<RadzenTestProj.Models.RbsDbContext.Session> CancelSessionChanges(RadzenTestProj.Models.RbsDbContext.Session item)
        {
            var entityToCancel = Context.Entry(item);
            if (entityToCancel.State == EntityState.Modified)
            {
              entityToCancel.CurrentValues.SetValues(entityToCancel.OriginalValues);
              entityToCancel.State = EntityState.Unchanged;
            }

            return item;
        }

        partial void OnSessionUpdated(RadzenTestProj.Models.RbsDbContext.Session item);
        partial void OnAfterSessionUpdated(RadzenTestProj.Models.RbsDbContext.Session item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Session> UpdateSession(long id, RadzenTestProj.Models.RbsDbContext.Session session)
        {
            OnSessionUpdated(session);

            var itemToUpdate = Context.Sessions
                              .Where(i => i.Id == session.Id)
                              .FirstOrDefault();

            if (itemToUpdate == null)
            {
               throw new Exception("Item no longer available");
            }
                
            var entryToUpdate = Context.Entry(itemToUpdate);
            entryToUpdate.CurrentValues.SetValues(session);
            entryToUpdate.State = EntityState.Modified;

            Context.SaveChanges();

            OnAfterSessionUpdated(session);

            return session;
        }

        partial void OnSessionDeleted(RadzenTestProj.Models.RbsDbContext.Session item);
        partial void OnAfterSessionDeleted(RadzenTestProj.Models.RbsDbContext.Session item);

        public async Task<RadzenTestProj.Models.RbsDbContext.Session> DeleteSession(long id)
        {
            var itemToDelete = Context.Sessions
                              .Where(i => i.Id == id)
                              .FirstOrDefault();

            if (itemToDelete == null)
            {
               throw new Exception("Item no longer available");
            }

            OnSessionDeleted(itemToDelete);


            Context.Sessions.Remove(itemToDelete);

            try
            {
                Context.SaveChanges();
            }
            catch
            {
                Context.Entry(itemToDelete).State = EntityState.Unchanged;
                throw;
            }

            OnAfterSessionDeleted(itemToDelete);

            return itemToDelete;
        }
        }
}