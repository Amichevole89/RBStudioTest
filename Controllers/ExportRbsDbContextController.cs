using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

using RadzenTestProj.Data;

namespace RadzenTestProj.Controllers
{
    public partial class ExportRbsDbContextController : ExportController
    {
        private readonly RbsDbContextContext context;
        private readonly RbsDbContextService service;

        public ExportRbsDbContextController(RbsDbContextContext context, RbsDbContextService service)
        {
            this.service = service;
            this.context = context;
        }

        [HttpGet("/export/RbsDbContext/accounts/csv")]
        [HttpGet("/export/RbsDbContext/accounts/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportAccountsToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetAccounts(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/accounts/excel")]
        [HttpGet("/export/RbsDbContext/accounts/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportAccountsToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetAccounts(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/campaignlocationevents/csv")]
        [HttpGet("/export/RbsDbContext/campaignlocationevents/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportCampaignLocationEventsToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetCampaignLocationEvents(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/campaignlocationevents/excel")]
        [HttpGet("/export/RbsDbContext/campaignlocationevents/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportCampaignLocationEventsToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetCampaignLocationEvents(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/campaignlocations/csv")]
        [HttpGet("/export/RbsDbContext/campaignlocations/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportCampaignLocationsToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetCampaignLocations(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/campaignlocations/excel")]
        [HttpGet("/export/RbsDbContext/campaignlocations/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportCampaignLocationsToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetCampaignLocations(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/campaigns/csv")]
        [HttpGet("/export/RbsDbContext/campaigns/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportCampaignsToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetCampaigns(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/campaigns/excel")]
        [HttpGet("/export/RbsDbContext/campaigns/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportCampaignsToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetCampaigns(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/contacts/csv")]
        [HttpGet("/export/RbsDbContext/contacts/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportContactsToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetContacts(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/contacts/excel")]
        [HttpGet("/export/RbsDbContext/contacts/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportContactsToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetContacts(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/contacttypes/csv")]
        [HttpGet("/export/RbsDbContext/contacttypes/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportContactTypesToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetContactTypes(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/contacttypes/excel")]
        [HttpGet("/export/RbsDbContext/contacttypes/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportContactTypesToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetContactTypes(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/establishments/csv")]
        [HttpGet("/export/RbsDbContext/establishments/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportEstablishmentsToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetEstablishments(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/establishments/excel")]
        [HttpGet("/export/RbsDbContext/establishments/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportEstablishmentsToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetEstablishments(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/eventtypes/csv")]
        [HttpGet("/export/RbsDbContext/eventtypes/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportEventTypesToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetEventTypes(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/eventtypes/excel")]
        [HttpGet("/export/RbsDbContext/eventtypes/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportEventTypesToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetEventTypes(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/locations/csv")]
        [HttpGet("/export/RbsDbContext/locations/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportLocationsToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetLocations(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/locations/excel")]
        [HttpGet("/export/RbsDbContext/locations/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportLocationsToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetLocations(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/people/csv")]
        [HttpGet("/export/RbsDbContext/people/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportPeopleToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetPeople(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/people/excel")]
        [HttpGet("/export/RbsDbContext/people/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportPeopleToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetPeople(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/sessions/csv")]
        [HttpGet("/export/RbsDbContext/sessions/csv(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportSessionsToCSV(string fileName = null)
        {
            return ToCSV(ApplyQuery(await service.GetSessions(), Request.Query, false), fileName);
        }

        [HttpGet("/export/RbsDbContext/sessions/excel")]
        [HttpGet("/export/RbsDbContext/sessions/excel(fileName='{fileName}')")]
        public async Task<FileStreamResult> ExportSessionsToExcel(string fileName = null)
        {
            return ToExcel(ApplyQuery(await service.GetSessions(), Request.Query, false), fileName);
        }
    }
}
