using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("Accounts", Schema = "public")]
    public partial class Account
    {
        [Key]
        [Required]
        public Guid PersonId { get; set; }

        public Person Person { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string HashedPassword { get; set; }

        [ConcurrencyCheck]
        public bool Active { get; set; }

        [ConcurrencyCheck]
        public bool Internal { get; set; }

        public ICollection<Session> Sessions { get; set; }

    }
}