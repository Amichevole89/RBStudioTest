using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("CampaignLocationEvents", Schema = "public")]
    public partial class CampaignLocationEvent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int CampaignLocationId { get; set; }

        public CampaignLocation CampaignLocation { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int ParticipantCount { get; set; }

        [ConcurrencyCheck]
        public DateTime? Entered { get; set; }

        [ConcurrencyCheck]
        public DateTime? Invoiced { get; set; }

        [ConcurrencyCheck]
        public DateTime? Paid { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int EventTypeId { get; set; }

        public EventType EventType { get; set; }

        [ConcurrencyCheck]
        public string Notes { get; set; }

    }
}