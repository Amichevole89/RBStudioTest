using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("People", Schema = "public")]
    public partial class Person
    {
        [Key]
        [Required]
        public Guid Id { get; set; }

        [ConcurrencyCheck]
        public string Name { get; set; }

        [ConcurrencyCheck]
        public string Email { get; set; }

        public ICollection<Account> Accounts { get; set; }

        public ICollection<Contact> Contacts { get; set; }

    }
}