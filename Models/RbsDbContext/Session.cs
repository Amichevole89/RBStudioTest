using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("Sessions", Schema = "public")]
    public partial class Session
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        [ConcurrencyCheck]
        public Guid AccountId { get; set; }

        public Account Account { get; set; }

        [Required]
        [ConcurrencyCheck]
        public Guid Jti { get; set; }

        [Required]
        [ConcurrencyCheck]
        public Guid RefreshToken { get; set; }

        [ConcurrencyCheck]
        public bool Active { get; set; }

        [ConcurrencyCheck]
        public DateTime Created { get; set; }

    }
}