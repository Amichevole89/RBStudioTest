using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("Establishments", Schema = "public")]
    public partial class Establishment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string Name { get; set; }

        public ICollection<Contact> Contacts { get; set; }

        public ICollection<Location> Locations { get; set; }

    }
}