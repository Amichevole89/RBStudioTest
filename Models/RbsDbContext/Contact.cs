using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("Contacts", Schema = "public")]
    public partial class Contact
    {
        [Key]
        [Required]
        public Guid PersonId { get; set; }

        public Person Person { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int EstablishmentId { get; set; }

        public Establishment Establishment { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int ContactTypeId { get; set; }

        public ContactType ContactType { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string Phone { get; set; }

        [ConcurrencyCheck]
        public bool Active { get; set; }

    }
}