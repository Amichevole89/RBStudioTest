using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("Locations", Schema = "public")]
    public partial class Location
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int EstablishmentId { get; set; }

        public Establishment Establishment { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string Name { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string Address { get; set; }

        [ConcurrencyCheck]
        public string City { get; set; }

        [ConcurrencyCheck]
        public string Address_state { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string PostalCode { get; set; }

        public ICollection<CampaignLocation> CampaignLocations { get; set; }

    }
}