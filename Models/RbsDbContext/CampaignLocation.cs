using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("CampaignLocations", Schema = "public")]
    public partial class CampaignLocation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int LocationId { get; set; }

        public Location Location { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int CampaignId { get; set; }

        public Campaign Campaign { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string Notes { get; set; }

        public ICollection<CampaignLocationEvent> CampaignLocationEvents { get; set; }

    }
}