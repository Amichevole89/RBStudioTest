using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("Campaigns", Schema = "public")]
    public partial class Campaign
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string Name { get; set; }

        [Required]
        [ConcurrencyCheck]
        public DateTime StartDate { get; set; }

        [Required]
        [ConcurrencyCheck]
        public DateTime EndDate { get; set; }

        public ICollection<CampaignLocation> CampaignLocations { get; set; }

        public ICollection<EventType> EventTypes { get; set; }

    }
}