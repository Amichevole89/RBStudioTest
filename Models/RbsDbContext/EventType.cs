using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RadzenTestProj.Models.RbsDbContext
{
    [Table("EventTypes", Schema = "public")]
    public partial class EventType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ConcurrencyCheck]
        public int CampaignId { get; set; }

        public Campaign Campaign { get; set; }

        [Required]
        [ConcurrencyCheck]
        public string Name { get; set; }

        [Required]
        [ConcurrencyCheck]
        public decimal Cost { get; set; }

        [Required]
        [ConcurrencyCheck]
        public decimal Donation { get; set; }

        [Required]
        [ConcurrencyCheck]
        public decimal MinimumDonation { get; set; }

        public ICollection<CampaignLocationEvent> CampaignLocationEvents { get; set; }

    }
}